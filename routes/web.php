<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => ['auth']], function (){
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('/shoes', 'ShoeController', ['except' => ['show']]);
    Route::resource('/providers', 'ProviderController', ['except' => ['show']]);
    Route::resource('/cashsales', 'CashSaleController', ['except' => ['show']]);
    Route::resource('/creditsales', 'CreditSaleController', ['except' => ['show']]);
    Route::get('/shoe_report/report', 'ShoeReportController@index')->name('report');
    Route::get('/shoe_report/report/pdf', 'ShoeReportController@pdf');
    Route::get('/sale_report/report', 'SaleReportController@index')->name('sale_report');
    Route::get('/sale_report/report/pdf', 'SaleReportController@pdf');
    Route::get('/gain_report/report', 'reportgainController@index')->name('report2');
    Route::get('/shoe_report/report/pdf', 'reportgainController@pdf');
});

