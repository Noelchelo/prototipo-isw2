# Shoe store
Administrative management system for a shoe store with Laravel created by students from Universidad Técnica Nacional


## Getting Started
* 1) Execute the command "composer install"
* 2) Execute the command "php artisan key:generate"
* 3) Configure your .env file
* 4) Execute the command "php artisan migrate"
* 5) Execute the command "php artisan serve"


## Contributors

* **[Noel Argüello Solano](https://gitlab.com/Noelchelo)**
* **[Marco Artavia Sandoval](https://gitlab.com/martavia)**
* **[Keilor Rivera](https://gitlab.com/Chakka28)**
* **[Kevin Rodriguez Jimenez](https://gitlab.com/KevinpumaRJ)**

## Built With
* [Bootstrap](https://getbootstrap.com) - CSS Framework
* [Laravel](https://laravel.com) - PHP Framework


## Versioning
[GitLab](https://gitlab.com/) for versioning. For the versions available, see the [Repository](https://gitlab.com/Noelchelo/prototipo-isw2).
