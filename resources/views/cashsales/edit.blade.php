@extends('layouts.app')

@section('content')
<div class="container">
    <form method="POST" action="{{ route('cashsales.update', $sale_count) }}">
        @csrf
        @method('PUT')
        <h1 class="text-center">Editar venta</h1>

        <div class="form-group row">
            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

            <div class="col-md-6">
                <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name"
                    placeholder="Nombre Aquí" required name="name" value="{{ $cashsales->name }}">
                @if ($errors->has('name'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="lastname" class="col-md-4 col-form-label text-md-right">{{ __('Apellidos') }}</label>

            <div class="col-md-6">
                <input type="text" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" id="lastname"
                    placeholder="Apellidos Aquí" required name="lastname" value="{{ $cashsales->lastname }}">
                @if ($errors->has('lastname'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('lastname') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
                <label for="shoe" class="col-md-4 col-form-label text-md-right">{{ __('Zapato') }}</label>

                <div class="col-md-6">
                    <input type="text" class="form-control{{ $errors->has('shoe') ? ' is-invalid' : '' }}" id="shoe"
                          required name="shoe" value="{{ $cashsales->shoe }}" readonly>
                    @if ($errors->has('shoe'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('shoe') }}</strong>
                    </span>
                    @endif
                </div>
            </div>

        <div class="form-group row">
            <label for="price" class="col-md-4 col-form-label text-md-right">{{ __('Precio') }}</label>

            <div class="col-md-6">
                <input type="text" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" id="price"
                    placeholder="Cantidad" required name="price" value="{{ $cashsales->price }}">
                @if ($errors->has('price'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('price') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Guardar') }}
                </button>
            </div>
        </div>
    </form>
</div>
@endsection
