@extends('layouts.app')

@section('content')
<div class="container">
    <form method="POST" action="{{ route('cashsales.store') }}">
        @csrf
        <h1 class="text-center">Añadir venta</h1>

        <div class="form-group row">
            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

            <div class="col-md-6">
                <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name"
                    placeholder="Nombre Aquí" required name="name" value="{{ old('name') }}">
                @if ($errors->has('name'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="lastname" class="col-md-4 col-form-label text-md-right">{{ __('Apellidos') }}</label>

            <div class="col-md-6">
                <input type="text" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" id="lastname"
                    placeholder="Apellidos Aquí" required name="lastname" value="{{ old('lastname') }}">
                @if ($errors->has('lastname'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('lastname') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="contact" class="col-md-4 col-form-label text-md-right">{{ __('Teléfono') }}</label>

            <div class="col-md-6">
                <input type="number" class="form-control{{ $errors->has('contact') ? ' is-invalid' : '' }}" id="contact"
                    placeholder="Teléfono Aquí" required name="contact" value="{{ old('contact') }}">
                @if ($errors->has('contact'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('contact') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="shoe" class="col-md-4 col-form-label text-md-right">{{ __('Zapato:') }}</label>
            <div class="col-md-6">
            <select id="shoe" name="shoe" class="form-control">
                    @if (count($shoes)>0)
                        @foreach ($shoes as $shoe)
                        <option value="{{ $shoe->name }}">{{ $shoe->name}} : {{$shoe->price}}</option>
                        @endforeach
                    @endif
            </select>
            </div>

        </div>

        <div class="form-group row">
            <label for="price" class="col-md-4 col-form-label text-md-right">{{ __('Precio') }}</label>

            <div class="col-md-6">
                <input type="text" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" id="price"
                    placeholder="Cantidad" required name="price" value="{{ old('price') }}">
                @if ($errors->has('price'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('price') }}</strong>
                </span>
                @endif
            </div>
        </div>


        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Guardar') }}
                </button>
            </div>
        </div>
    </form>
</div>
@endsection
