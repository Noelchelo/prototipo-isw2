@extends('layouts.app')
@section('content')
<div class="container">
    <h1>Venta por contado</h1>
    <a class="btn btn-success mb-2" href="{{ route('cashsales.create') }}">Nueva venta</a>
    @if (count($cashsales)>0)
    <table class="table table-striped table-bordered">
        <tr>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Teléfono</th>
            <th>Zapato</th>
            <th>Precio zapato</th>
        </tr>
        @foreach ($cashsales as $item)
        <tr>
            <td>{{ $item->name }}</td>
            <td>{{ $item->lastname }}</td>
            <td>{{ $item->contact }}</td>
            <td>{{ $item->shoe }}</td>
            <td>{{ $item->price }}</td>
        </tr>
        @endforeach
    </table>
    @else
    <li>Datos no disponibles</li>
    @endif
</div>
@endsection
