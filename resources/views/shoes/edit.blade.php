@extends('layouts.app')

@section('content')
<div class="container">
    <form method="POST" action="{{ route('shoes.update', $shoe) }}">
        @csrf
        @method('PUT')
        <h1 class="text-center">Editar Zapato</h1>

        <div class="form-group row mt-5">
            <label for="code" class="col-md-4 col-form-label text-md-right">{{ __('Código') }}</label>

            <div class="col-md-6">
                <input type="number" required class="form-control{{ $errors->has('code') ? ' is-invalid' : '' }}"
                    id="code" placeholder="Código Aquí" name="code" value="{{ $shoe->code }}">
                @if ($errors->has('code'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('code') }}</strong>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

            <div class="col-md-6">
                <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name"
                    placeholder="Nombre Aquí" required name="name" value="{{ $shoe->name }}">
                @if ($errors->has('name'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="price" class="col-md-4 col-form-label text-md-right">{{ __('Precio') }}</label>

            <div class="col-md-6">
                <input type="text" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" id="price"
                    placeholder="Precio Aquí" required name="price" value="{{ $shoe->price }}">
                @if ($errors->has('name'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('price') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Guardar') }}
                </button>
            </div>
        </div>
    </form>
</div>
@endsection