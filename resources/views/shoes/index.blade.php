@extends('layouts.app')
@section('content')
<div class="container">
    <h1>Zapatos</h1>
    <a class="btn btn-success mb-2" href="{{ route('shoes.create') }}">Nuevo Zapato</a>
    @if (count($shoes)>0)
    <table class="table table-striped table-bordered">
        <tr>
            <th>Codigo</th>
            <th>Nombre</th>
            <th>Precio</th>
            <th>Acciones</th>
        </tr>
        @foreach ($shoes as $shoe)
        <tr>
            <td>{{ $shoe->code }}</td>
            <td>{{ $shoe->name }}</td>
            <td>{{ $shoe->price }}</td>
            <td>
                <div class="form-row">
                    <div class="col-auto">
                        <a href="/shoes/{{$shoe->id}}/edit " class="btn btn-primary">Editar</a>
                    </div>
                    <div class="col-auto">
                        <form method="POST" action="{{ route('shoes.destroy', $shoe) }}">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger">Eliminar</button>
                        </form>
                    </div>
                </div>
            </td>
        </tr>
        @endforeach
    </table>
    @else
    <li>Datos no disponibles</li>
    @endif
</div>
@endsection