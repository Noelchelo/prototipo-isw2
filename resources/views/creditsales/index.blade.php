@extends('layouts.app')
@section('content')
<div class="container">
    <h1>Venta a credito</h1>
    <a class="btn btn-success mb-2" href="{{ route('creditsales.create') }}">Nueva venta</a>
    @if (count($creditsales)>0)
    <table class="table table-striped table-bordered">
        <tr>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Teléfono</th>
            <th>Zapato</th>
            <th>Precio Zapato</th>
            <th>Abonado</th>
            <th>Saldo actual</th>
            <th>Acciones</th>
        </tr>
        @foreach ($creditsales as $item)
        @if($item->state==false)
        <tr>
            <td>{{ $item->name }}</td>
            <td>{{ $item->lastname }}</td>
            <td>{{ $item->contact }}</td>
            <td>{{ $item->shoe }}</td>
            <td>{{ $item->price }}</td>
            <td>{{ $item->mont }}</td>
            <td>{{ $item->balance }}</td>

            <td>
                <div class="form-row">
                    <div class="col-auto">
                        <a href="/creditsales/{{$item->id}}/edit " class="btn btn-primary">Editar</a>
                    </div>
                </div>
            </td>
        </tr>
        @endif
        @endforeach
    </table>
    @else
    <li>Datos no disponibles</li>
    @endif
</div>
@endsection
