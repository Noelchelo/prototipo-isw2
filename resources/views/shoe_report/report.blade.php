
@extends('layouts.app')
@section('content')
  <div class="container">
   <h3 align="center">Reporte de zapatos</h3><br />
   
   <div class="row">
    <div class="col-md-7">
    </div>
    <div class="col-md-5" align="right">
     <a href="{{ url('/shoe_report/report/pdf') }}" class="btn btn-danger">Convertir en PDF</a>
    </div>
   </div>
   <br />
   <div class="table-responsive">
    <table class="table table-striped table-bordered">
     <thead>
      <tr>
       <th>Código</th>
       <th>Nombre</th>
       <th>Precio</th>
      </tr>
     </thead>
     <tbody>
     @foreach($shoes as $shoe)
      <tr>
       <td>{{ $shoe->code }}</td>
       <td>{{ $shoe->name }}</td>
       <td>{{ $shoe->price }}</td>
      </tr>
     @endforeach
     </tbody>
    </table>
   </div>
  </div>
  @endsection