@extends('layouts.app')
@section('content')
<?php $total = 0;
?>
  <div class="container">
   <h3 align="center">Reporte de ganancias</h3><br />
   
   <div class="row">
    <div class="col-md-7">
    </div>
    <div class="col-md-5" align="right">
     <a href="{{ url('/shoe_report/report/pdf') }}" class="btn btn-danger">Convertir en PDF</a>
    </div>
   </div>
   <br />
   <div class="table-responsive">
    <table class="table table-striped table-bordered">
     <thead>
      <tr>
       <th>Nombre cliente</th>
       <th>Zapato</th>
       <th>Cantidad</th>
       <th>Tipo de venta</th>
      </tr>
     </thead>
     <tbody>
     @foreach($cashsales as $cash)
     @php 
     $total += $cash->price;
    @endphp
      <tr>
       <td>{{ $cash->name}}</td>
       <td>{{ $cash->shoe}}</td>
       <td>{{ $cash->price}}</td>
       <td>Contado</td>
      </tr>
     @endforeach

     @foreach($creditsales as $credit)
        @if($credit->state == true)
        @php 
        $total += $credit->price;
        @endphp
            <tr>
            <td>{{ $credit->name}}</td>
            <td>{{ $credit->shoe}}</td>
            <td>{{ $credit->price}}</td>
            <td>Credito</td>
            </tr>
        @endif
     @endforeach

     </tbody>
    </table>
    <h4>Total:{{$total}}</h4>
   </div>
  </div>
  @endsection