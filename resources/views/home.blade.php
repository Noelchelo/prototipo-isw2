@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h2 style="color:black;">Registros</h2>
            <br>
            <div class="form-group">

                <a class="btn btn-primary btn-lg" href="{{route('shoes.index')}}" style="width:170px;height:170px;"><img src="/iconos/iconfinder.png" style="width:120px;height:120px;"><br>
                    <label class="text-center" style="font-size:17px;">Zapatos</label></a>

                <a class="btn btn-primary btn-lg" href="{{route('providers.index')}}" style="width:170px;height:170px;"><img src="/iconos/proovedor.png" style="width:120px;height:120px;"><br>
                    <label class="text-center" style="font-size:17px;">Proovedor</label></a>

                <a class="btn btn-primary btn-lg" href="{{route('creditsales.index')}}" style="width:170px;height:170px;"><img src="/iconos/credito.png" style="width:120px;height:120px;"><br>
                    <label class="text-center" style="font-size:17px;">Crédito</label></a>

                <a class="btn btn-primary btn-lg" href="{{route('cashsales.index')}}" style="width:170px;height:170px;"><img src="/iconos/contado.png" style="width:120px;height:120px;"><br>
                    <label class="text-center" style="font-size:17px;">Contado</label></a>

            </div>
            <br>
            <h2 style="color:black;">Reportes</h2>
            <br>
            <div>
                <a href="{{route('report')}}"><button type="button" class="btn btn-primary btn-lg" style="width:170px;height:170px;"><img src="/iconos/reportezapatos.png" style="width:120px;height:120px;"><br>
                        <label class="text-center" style="font-size:17px;">Zapatos</label></button></a>

                <a href="{{route('sale_report')}}" class="btn btn-primary btn-lg" style="width:170px;height:170px;"><img src="/iconos/registroventas.png" style="width:120px;height:120px;"><br>
                    <label class="text-center" style="font-size:17px;">Ventas</label></a>


                <a href="{{route('report2')}}"><button type="button" class="btn btn-primary btn-lg" style="width:170px;height:170px;"><img src="/iconos/ganancias.png" style="width:120px;height:120px;"><br>
                        <label class="text-center" style="font-size:17px;">Ganancias</label></button></a>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
