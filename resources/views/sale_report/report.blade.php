@extends('layouts.app')
@section('content')
  <div class="container">
    <h3 align="center">Reporte de ventas</h3><br />

   <div class="row">
    <div class="col-md-7">
    </div>
    <div class="col-md-5" align="right">
     <a href="{{ url('/sale_report/report/pdf') }}" class="btn btn-danger">Convertir en PDF</a>
    </div>
   </div>
   <br />
   <div class="table-responsive">
    <h4 class="text-left" style="color:black;">Contado</h4>
    <table class="table table-striped table-bordered">
     <thead>
      <tr>
       <th>Nombre</th>
       <th>Apellidos</th>
       <th>Teléfono</th>
       <th>Zapato</th>
       <th>Precio</th>
      </tr>
     </thead>
     <tbody>
     @foreach($cashsales as $cashsale)
      <tr>
       <td>{{ $cashsale->name }}</td>
       <td>{{ $cashsale->lastname }}</td>
       <td>{{ $cashsale->contact }}</td>
       <td>{{ $cashsale->shoe }}</td>
       <td>{{ $cashsale->price }}</td>
      </tr>
     @endforeach
     </tbody>
    </table>
   </div>
   <div class="table-responsive" style="margin-top: 100px;">
    <h4 class="text-left" style="color:black;">Crédito</h4>
    <table class="table table-striped table-bordered">
     <thead>
      <tr>
       <th>Nombre</th>
       <th>Apellidos</th>
       <th>Teléfono</th>
       <th>Zapato</th>
       <th>Precio</th>
       <th>Abonado</th>
       <th>Saldo</th>
       <th>Estado</th>

      </tr>
     </thead>
     <tbody>
     @foreach($creditsales as $creditsale)
      <tr>
       <td>{{ $creditsale->name }}</td>
       <td>{{ $creditsale->lastname }}</td>
       <td>{{ $creditsale->contact }}</td>
       <td>{{ $creditsale->shoe }}</td>
       <td>{{ $creditsale->price }}</td>
       <td>{{ $creditsale->mont }}</td>
       <td>{{ $creditsale->balance }}</td>
       @if($creditsale->state)
       <td>Vendido</td>
       @else
       <td>Debe</td>
       @endif
      </tr>
     @endforeach
     </tbody>
    </table>
   </div>
  </div>
 </body>
</html>
@endsection
