@extends('layouts.app')
@section('content')
<div class="container">
    <h1>Proveedores</h1>
    <a class="btn btn-success mb-2" href="{{ route('providers.create') }}">Nuevo Proveedor</a>
    @if (count($providers)>0)
    <table class="table table-striped table-bordered">
        <tr>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Telefono</th>
            <th>Acciones</th>
        </tr>
        @foreach ($providers as $provider)
        <tr>
            <td>{{ $provider->name }}</td>
            <td>{{ $provider->lastname }}</td>
            <td>{{ $provider->phone }}</td>
            <td>
                <div class="form-row">
                    <div class="col-auto">
                        <a href="/providers/{{$provider->id}}/edit " class="btn btn-primary">Editar</a>
                    </div>
                    <div class="col-auto">
                        <form method="POST" action="{{ route('providers.destroy', $provider) }}">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger">Eliminar</button>
                        </form>
                    </div>
                </div>
            </td>
        </tr>
        @endforeach
    </table>
    @else
    <li>Datos no disponibles</li>
    @endif
</div>
@endsection
