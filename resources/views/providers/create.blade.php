@extends('layouts.app')

@section('content')
<div class="container">
    <form method="POST" action="{{ route('providers.store') }}">
        @csrf
        <h1 class="text-center">Añadir Proveedor</h1>

        <div class="form-group row">
            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

            <div class="col-md-6">
                <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name"
                    placeholder="Nombre Aquí" required name="name" value="{{ old('name') }}">
                @if ($errors->has('name'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                @endif
            </div>
        </div>
        
        <div class="form-group row">
            <label for="lastname" class="col-md-4 col-form-label text-md-right">{{ __('Apellidos') }}</label>

            <div class="col-md-6">
                <input type="text" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" id="lastname"
                    placeholder="Apellidos Aquí" required name="lastname" value="{{ old('lastname') }}">
                @if ($errors->has('lastname'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('lastname') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Teléfono') }}</label>

            <div class="col-md-6">
                <input type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" id="phone"
                    placeholder="Teléfono Aquí" required name="phone" value="{{ old('phone') }}">
                @if ($errors->has('phone'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('phone') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    {{ __('Guardar') }}
                </button>
            </div>
        </div>
    </form>
</div>
@endsection