<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CreditSale extends Model
{
    protected $fillable = ['name', 'lastname','contact', 'shoe', 'price','mont','balance', 'state'];
}
