<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CashSale extends Model
{
    protected $fillable = ['name', 'lastname', 'contact', 'shoe', 'price'];
}
