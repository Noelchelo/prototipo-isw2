<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CashSalesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
            'contact' => ['required', 'numeric', 'max:10'],
            'shoe' => ['required', 'string', 'max:255'],
            'price' => ['required', 'numeric'],
        ];
    }
}
