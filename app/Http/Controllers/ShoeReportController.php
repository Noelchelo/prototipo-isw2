<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use PDF;

class ShoeReportController extends Controller
{
    function index()
    {
     $shoes = $this->get_shoe();
     return view('/shoe_report/report')->with('shoes', $shoes);
    }

    function get_shoe()
    {
     $shoes = DB::table('shoes')
         ->limit(10)
         ->get();
     return $shoes;
    }

    function pdf()
    {
     $pdf = \App::make('dompdf.wrapper');
     $pdf->loadHTML($this->convert_shoes_to_html());
     return $pdf->stream();
    }

    function convert_shoes_to_html()
    {
     $shoes = $this->get_shoe();
     $output = '
     <h3 align="center">Zapatos</h3>
     <table width="100%" style="border-collapse: collapse; border: 0px;">
      <tr>
    <th style="border: 1px solid; padding:12px;" width="20%">Código</th>
    <th style="border: 1px solid; padding:12px;" width="30%">Nombre</th>
    <th style="border: 1px solid; padding:12px;" width="15%">Precio</th>
   </tr>
     ';  
     foreach($shoes as $shoe)
     {
      $output .= '
      <tr>
       <td style="border: 1px solid; padding:12px;">'.$shoe->code.'</td>
       <td style="border: 1px solid; padding:12px;">'.$shoe->name.'</td>
       <td style="border: 1px solid; padding:12px;">'.$shoe->price.'</td>
      </tr>
      ';
     }
     $output .= '</table>';
     return $output;
    }
}
