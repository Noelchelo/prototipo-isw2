<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use PDF;

class SaleReportController extends Controller
{
    function index()
    {
     $cashsales = $this->get_cashSale();
     $creditsales = $this->get_creditSale();
     return view('/sale_report/report', compact('cashsales', 'creditsales'));
    }

    function get_cashSale()
    {
     $cashsales = DB::table('cash_sales')
         ->limit(10)
         ->get();
     return $cashsales;
    }
    function get_creditSale()
    {
     $creditsales = DB::table('credit_sales')
         ->limit(10)
         ->get();
     return $creditsales;
    }

    function pdf()
    {
     $pdf = \App::make('dompdf.wrapper');
     $pdf->loadHTML($this->convert_sales_to_html());
     return $pdf->stream();
    }

    function convert_sales_to_html()
    {
     $cashsales = $this->get_cashSale();
     $output = '
     <h3 align="center">Ventas por contado</h3>
     <table width="100%" style="border-collapse: collapse; border: 0px;">
      <tr>
    <th style="border: 1px solid; padding:12px;" width="20%">Nombre</th>
    <th style="border: 1px solid; padding:12px;" width="20%">Apellidos</th>
    <th style="border: 1px solid; padding:12px;" width="20%">Telefono</th>
    <th style="border: 1px solid; padding:12px;" width="20%">Zapato</th>
    <th style="border: 1px solid; padding:12px;" width="20%">Precio</th>
   </tr>
     ';
     foreach($cashsales as $cashsale)
     {
      $output .= '
      <tr>
       <td style="border: 1px solid; padding:12px;">'.$cashsale->name.'</td>
       <td style="border: 1px solid; padding:12px;">'.$cashsale->lastname.'</td>
       <td style="border: 1px solid; padding:12px;">'.$cashsale->contact.'</td>
       <td style="border: 1px solid; padding:12px;">'.$cashsale->shoe.'</td>
       <td style="border: 1px solid; padding:12px;">'.$cashsale->price.'</td>
      </tr>
      ';
     }
     $output .= '</table>';

     $creditsales = $this->get_creditSale();
     $output .= '
     <h3 align="center">Ventas a credito</h3>
     <table width="100%" style="border-collapse: collapse; border: 0px;">
      <tr>
    <th style="border: 1px solid; padding:12px;" width="12.5%">Nombre</th>
    <th style="border: 1px solid; padding:12px;" width="12.5%">Apellidos</th>
    <th style="border: 1px solid; padding:12px;" width="12.5%">Telefono</th>
    <th style="border: 1px solid; padding:12px;" width="12.5%">Zapato</th>
    <th style="border: 1px solid; padding:12px;" width="12.5%">Precio</th>
    <th style="border: 1px solid; padding:12px;" width="12.5%">Abonado</th>
    <th style="border: 1px solid; padding:12px;" width="12.5%">Saldo</th>
    <th style="border: 1px solid; padding:12px;" width="12.5%">Estado</th>

   </tr>
     ';
     foreach($creditsales as $creditsale)
     {
         $estado="";
         if($creditsale->state){
             $estado ="Vendido";
         }else{
             $estado="Debe";
         }
      $output .= '
      <tr>
       <td style="border: 1px solid; padding:12px;">'.$creditsale->name.'</td>
       <td style="border: 1px solid; padding:12px;">'.$creditsale->lastname.'</td>
       <td style="border: 1px solid; padding:12px;">'.$creditsale->contact.'</td>
       <td style="border: 1px solid; padding:12px;">'.$creditsale->shoe.'</td>
       <td style="border: 1px solid; padding:12px;">'.$creditsale->price.'</td>
       <td style="border: 1px solid; padding:12px;">'.$creditsale->mont.'</td>
       <td style="border: 1px solid; padding:12px;">'.$creditsale->balance.'</td>
       <td style="border: 1px solid; padding:12px;">'.$estado.'</td>
      </tr>
      ';
     }
     $output .= '</table>';
     return $output;
    }
}
