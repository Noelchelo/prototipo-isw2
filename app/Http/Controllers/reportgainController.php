<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CashSale;
use App\Models\CreditSale;
use App\Models\Shoe;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;

class reportgainController extends Controller
{
    function index()
    {
        $creditsales = CreditSale::all();
        $cashsales = CashSale::all();

        return view('/gain_report/report')->with('creditsales', $creditsales)->with('cashsales', $cashsales);;
    }

    function pdf()
    {
     $pdf = \App::make('dompdf.wrapper');
     $pdf->loadHTML($this->convert_shoes_to_html());
     return $pdf->stream();
    }

    function convert_shoes_to_html()
    {
     $creditsales = CreditSale::all();
     $cashsales = CashSale::all();
     $total = 0;
     $output = '
     <h3 align="center">Reporte de ganancias</h3>
     <table width="100%" style="border-collapse: collapse; border: 0px;">
      <tr>
    <th style="border: 1px solid; padding:12px;" width="20%">Nombre</th>
    <th style="border: 1px solid; padding:12px;" width="30%">Zapato</th>
    <th style="border: 1px solid; padding:12px;" width="30%">Precio</th>
    <th style="border: 1px solid; padding:12px;" width="30%">Tipo venta</th>
   </tr>
     ';  
     foreach($cashsales as $cash)
     {
        $total += $cash->price;
      $output .= '
      <tr>
       <td style="border: 1px solid; padding:12px;">'.$cash->name.'</td>
       <td style="border: 1px solid; padding:12px;">'.$cash->shoe.'</td>
       <td style="border: 1px solid; padding:12px;">'.$cash->price.'</td>
       <td style="border: 1px solid; padding:12px;">Contado</td>
      </tr>
      ';
     }
     foreach($creditsales as $cash)
     {
         if($cash->state == true)
         {
            $total += $cash->price;
            $output .= '
            <tr>
            <td style="border: 1px solid; padding:12px;">'.$cash->name.'</td>
            <td style="border: 1px solid; padding:12px;">'.$cash->shoe.'</td>
            <td style="border: 1px solid; padding:12px;">'.$cash->price.'</td>
            <td style="border: 1px solid; padding:12px;">Credito</td>
            </tr>
            ';
         }
     }
     $output .= '<h3 align="center">Ganancias:'.$total.'</h3>';
     $output .= '';
     return $output;
    }
}
