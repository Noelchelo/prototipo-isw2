<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CreditSale;
use App\Models\Shoe;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\CreditSalesRequest;

class CreditSaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $creditsales = CreditSale::all();
        return view('creditsales.index', compact('creditsales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $shoes = Shoe::all();
        return view('creditsales.create', compact('shoes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $mont = $request->get('mont');
        $balance = $request->get('price') - $mont;

        $request["balance"]=$balance;
        CreditSale::create($request->all());



        return redirect('/creditsales');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $creditsales = CreditSale::find($id);
        $shoes = Shoe::all();
        return view('creditsales.edit', compact('creditsales','shoes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $creditsales = CreditSale::find($id);
        $mont = $request->get('mont');
        $balance = $creditsales->balance - $mont;
        $mont += $creditsales->mont;
        $request["mont"]=$mont;

        $request["balance"]=$balance;

        if($balance == 0){
            $request["state"] = true;
        }

        $creditsales->update($request->all());
        return redirect('/creditsales');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CreditSale::destroy($id);
        return redirect('/creditsales');
    }
}
